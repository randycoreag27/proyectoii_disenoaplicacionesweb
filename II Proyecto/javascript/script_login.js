let name_user;

function login_validation() {   

    //Extrae la informacion de los campos de texto
    const u = document.getElementById("username").value;
    const p = document.getElementById("pass").value;

    let list_users = JSON.parse(localStorage.getItem('list_users'));
    let access = false;

    list_users.forEach(user => {
        if(u == user.username && p == user.password){
            access = true;
            name_user = user.name + " " + user.surnames;
            sessionStorage.setItem("usuario_activo", user.username);
        }
    });

    if(access == true)
    {
        window.location = "../dashbord.html"
        alert("¡Hola " + name_user + "!");
    }
    else if(access == false)
    {
        alert("¡Error. Datos incorrectos!");
        clean_fields(); 
    }
    
}

function clean_fields(){
    const username = document.getElementById("username").value = "";
    const password = document.getElementById("pass").value = "";
}