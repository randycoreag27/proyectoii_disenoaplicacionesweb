fill_table();

function fill_table(){
    
    var list_rides = JSON.parse(localStorage.getItem("list_rides"));

    var table = document.getElementById("table_ride");

    table.innerHTML = "";

    list_rides.forEach(ride => {
        
        var row = document.createElement("tr");

        cell1 = document.createElement("td");
        cell2 = document.createElement("td");
        cell3 = document.createElement("td");
        cell4 = document.createElement("td");
        cell5 = document.createElement("td");
        cell6 = document.createElement("td");
        cell7 = document.createElement("td");

        cell1.innerHTML = ride.id;
        cell2.innerHTML = ride.ride_name;
        cell3.innerHTML = ride.from_ride;
        cell4.innerHTML = ride.to_ride;
        cell5.innerHTML = ride.departure_time;
        cell6.innerHTML = ride.arrival_time;
        cell7.innerHTML = ride.description;
        
        row.appendChild(cell1);
        row.appendChild(cell2);
        row.appendChild(cell3);
        row.appendChild(cell4);
        row.appendChild(cell5);
        row.appendChild(cell6);
        row.appendChild(cell7);

        table.appendChild(row);

    });

}

function search_ride(){

    const fr = document.getElementById("salida").value;
    const tr = document.getElementById("llegada").value;

    let list_rides = JSON.parse(localStorage.getItem('list_rides'));
    var table = document.getElementById("table_ride");
    var exists = false;

    table.innerHTML = "";

    if (fr === "" && tr === ""){
        alert("Los campos estan vacíos, por favor añada las ubicaciones");
        fill_table();
        clean_fields();
    }else{
        list_rides.forEach(ride => {

            if(fr == ride.from_ride && tr == ride.to_ride){

                var row = document.createElement("tr");

                cell1 = document.createElement("td");
                cell2 = document.createElement("td");
                cell3 = document.createElement("td");
                cell4 = document.createElement("td");
                cell5 = document.createElement("td");
                cell6 = document.createElement("td");
                cell7 = document.createElement("td");
        
                cell1.innerHTML = ride.id;
                cell2.innerHTML = ride.ride_name;
                cell3.innerHTML = ride.from_ride;
                cell4.innerHTML = ride.to_ride;
                cell5.innerHTML = ride.departure_time;
                cell6.innerHTML = ride.arrival_time;
                cell7.innerHTML = ride.description;
                
                row.appendChild(cell1);
                row.appendChild(cell2);
                row.appendChild(cell3);
                row.appendChild(cell4);
                row.appendChild(cell5);
                row.appendChild(cell6);
                row.appendChild(cell7);
        
                table.appendChild(row);

                exists = true;
                clean_fields();
            }
            
        });

        if (exists == false){
            alert("No existen rides disponibles para las ubicaciones ingresadas");
            fill_table();
            clean_fields();
        }

    }

}

function clean_fields(){
    const fr= document.getElementById("salida").value = "";
    const tr = document.getElementById("llegada").value = "";
}