function register(){

    const name = document.getElementById("name").value;
    const surnames = document.getElementById("surnames").value;
    const dateofbirth = document.getElementById("date_of_birth").value;
    const email = document.getElementById("email").value;
    const cellphonenubmer = document.getElementById("cellphone_nubmer").value;
    const username = document.getElementById("user").value;
    const password = document.getElementById("password").value;
    const repassword = document.getElementById("repassword").value;

    let list_users = JSON.parse(localStorage.getItem('list_users'));

    if(!list_users) {
        list_users = [];
    }

    const Usuarios =
    {
        id: (list_users.length + 1) - 1,
        name: name,
        surnames: surnames,
        dateofbirth: dateofbirth,
        email: email,
        cellphonenubmer: cellphonenubmer,
        username: username,
        password: password,
        repassword: repassword 
    };
    
    list_users.push(Usuarios);
    localStorage.setItem('list_users', JSON.stringify(list_users));

}

function validation(){

    const name = document.getElementById("name").value;
    const surnames = document.getElementById("surnames").value;
    const dateofbirth = document.getElementById("date_of_birth").value;
    const email = document.getElementById("email").value;
    const cellphonenubmer = document.getElementById("cellphone_nubmer").value;
    const username = document.getElementById("user").value;
    const password = document.getElementById("password").value;
    const repassword = document.getElementById("repassword").value;

    if (name === "" || email === "" || username === "" || password === "" || surnames === "" || cellphonenubmer === "" || repassword === ""){
        alert("¡ERROR! Hay espacios en blanco");
    }else {
        register();
        alert("Datos guardados correctamente");
        clean_fields();
    }

}

function clean_fields(){

    const name = document.getElementById("name").value = "";
    const surnames = document.getElementById("surnames").value = "";
    const dateofbirth = document.getElementById("date_of_birth").value = "";
    const email = document.getElementById("email").value = " ";
    const cellphonenubmer = document.getElementById("cellphone_nubmer").value = "";
    const username = document.getElementById("user").value = "";
    const password = document.getElementById("password").value = "";
    const repassword = document.getElementById("repassword").value = "";

}