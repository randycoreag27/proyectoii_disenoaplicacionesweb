delete_ride();

function delete_ride(){

    var list_rides = JSON.parse(localStorage.getItem("list_rides"));

    var table = document.getElementById("table_ride");

    table.innerHTML = "";

    list_rides.forEach(ride => {
        
        var row = document.createElement("tr");

        cell1 = document.createElement("td");
        cell2 = document.createElement("td");
        cell3 = document.createElement("td");
        cell4 = document.createElement("td");
        cell5 = document.createElement("td");
        cell6 = document.createElement("td");
        cell7 = document.createElement("td");
        cell8 = document.createElement("td");

        delete_button = document.createElement("button");
        delete_button.textContent = "Eliminar";
        cell8.appendChild(delete_button);

        cell1.innerHTML = ride.id;
        cell2.innerHTML = ride.ride_name;
        cell3.innerHTML = ride.from_ride;
        cell4.innerHTML = ride.to_ride;
        cell5.innerHTML = ride.departure_time;
        cell6.innerHTML = ride.arrival_time;
        cell7.innerHTML = ride.description;
        
        row.appendChild(cell1);
        row.appendChild(cell2);
        row.appendChild(cell3);
        row.appendChild(cell4);
        row.appendChild(cell5);
        row.appendChild(cell6);
        row.appendChild(cell7);
        row.appendChild(cell8);

        table.appendChild(row);

    });
}