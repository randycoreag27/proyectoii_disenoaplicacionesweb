fill_table_for_user();

function fill_table_for_user(){
    
    var list_rides = JSON.parse(localStorage.getItem("list_rides"));
    var table = document.getElementById("table_ride");
    var user = sessionStorage.getItem("usuario_activo");

    table.innerHTML = "";

    list_rides.forEach(ride => {

        if(user == ride.user){
            
            var row = document.createElement("tr");

            cell1 = document.createElement("td");
            cell2 = document.createElement("td");
            cell3 = document.createElement("td");
            cell4 = document.createElement("td");
            cell5 = document.createElement("td");
            cell6 = document.createElement("td");
            cell7 = document.createElement("td");
    
            cell1.innerHTML = ride.id;
            cell2.innerHTML = ride.ride_name;
            cell3.innerHTML = ride.from_ride;
            cell4.innerHTML = ride.to_ride;
            cell5.innerHTML = ride.departure_time;
            cell6.innerHTML = ride.arrival_time;
            cell7.innerHTML = ride.description;
            
            row.appendChild(cell1);
            row.appendChild(cell2);
            row.appendChild(cell3);
            row.appendChild(cell4);
            row.appendChild(cell5);
            row.appendChild(cell6);
            row.appendChild(cell7);
    
            table.appendChild(row);
        }
        
    });

}