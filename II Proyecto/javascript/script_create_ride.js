
function save_ride(){

    const ride_name = document.getElementById("ride_name").value;
    const from_ride = document.getElementById("from_ride").value;
    const to_ride = document.getElementById("to_ride").value;
    const description = document.getElementById("description").value;
    const departure_time= document.getElementById("departure_time").value;
    const arrival_time = document.getElementById("arrival_time").value;
    const user = sessionStorage.getItem("usuario_activo");

    let list_rides = JSON.parse(localStorage.getItem('list_rides'));

    if(!list_rides) {
        list_rides = [];
    }

    const rides =
    {
        id: (list_rides.length + 1) - 1,
        ride_name: ride_name,
        from_ride: from_ride,
        to_ride: to_ride,
        description: description,
        departure_time: departure_time,
        arrival_time: arrival_time,
        user: user
    };
    
    list_rides.push(rides);
    localStorage.setItem('list_rides', JSON.stringify(list_rides));

}

function validation(){

    const ride_name = document.getElementById("ride_name").value;
    const from_ride = document.getElementById("from_ride").value;
    const to_ride = document.getElementById("to_ride").value;
    const description = document.getElementById("description").value;

    if (ride_name === "" || from_ride === "" || to_ride === "" || description === ""){
        alert("¡Error! Hay espacios en blanco");
    }else {
        save_ride();
        alert("Datos guardados correctamente");
        clean_fields();
    }
}


function clean_fields(){

    const ride_name = document.getElementById("ride_name").value = "";
    const from_ride = document.getElementById("from_ride").value = "";
    const to_ride = document.getElementById("to_ride").value = "";
    const description = document.getElementById("description").value = "";
    const departure_time= document.getElementById("departure_time").value = "";
    const arrival_time = document.getElementById("arrival_time").value = "";
    const dy = document.querySelectorAll(".class").value = "";

}